{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![Chemical Engineering Logo](https://sutherland.che.utah.edu/Chem-Engingeering_combined-hrz.jpg)\n",
    "\n",
    "----\n",
    "# Arrays in Python\n",
    "\n",
    "Contributors:\n",
    " * [James C. Sutherland](sutherland.che.utah.edu)\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "__Note__: if you are a matlab user, you may want to look at\n",
    "  * This [great cheat sheet](http://mathesaurus.sourceforge.net/matlab-python-xref.pdf) showing common matlab commands and their python counterparts\n",
    "  * [Numpy for matlab users](http://mathesaurus.sourceforge.net/matlab-numpy.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Overview\n",
    "In python, there are a few ways to manage collections of information:\n",
    "  * A [`list`](#Lists) is built-in to the language, and can contain any type\n",
    "  * A [`tuple`](#Tuples) is also built-in to the language, and is similar to a list, but is immutable.\n",
    "  * A [numpy array](#Numpy-Arrays) is provided by the [`numpy`](https://docs.scipy.org/doc/numpy/reference) package in python and is useful for holding collections of numbers.\n",
    "  * A [dictionary](https://docs.python.org/3/tutorial/datastructures.html#dictionaries) provides a map - typically between strings and other things like strings, numbers, arrays, etc., and can be nested.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Lists\n",
    "A _list_ in python is enclosed by square brackets, and can contain any types (including other lists):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 'a string', 5, 3.14, [1, 2, 3, 4], ['apple', 'banana', 'orange']]\n",
      "4\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "# A list of integers:\n",
    "x = [1,2,3,4]\n",
    "\n",
    "# A list of strings:\n",
    "fruits = ['apple','banana','orange']\n",
    "\n",
    "# A list of many things, including other lists!\n",
    "my_list = [ 2, 'a string', 5, 3.14, x, fruits ]\n",
    "\n",
    "print(my_list)\n",
    "\n",
    "print(my_list.index(x))  # find where x appears (4)\n",
    "print(my_list[2])        # 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "You'll note from this example that python arrays are 0-based. The first element in the list is accessed via [0] while the second is [1], etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "You can think of a list in python as an array in Matlab, C/C++, Fortran, etc.  The primary difference is that, in Python, a list can have different types (strings, integeres, etc.) in the same list/array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "___Caution___: copying arrays in python is unique:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [1,2,3]\n",
    "b = a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "makes `b` alias to `a`.  This means that"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "b[1]=5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "also changes `a[1]` to 5:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 5, 3] [1, 5, 3]\n"
     ]
    }
   ],
   "source": [
    "print(a,b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To make a full copy of an array, use the `copy` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 5, 3] [1, 5, 3] [1, 5, 19]\n"
     ]
    }
   ],
   "source": [
    "b = a         # b and a are the same array\n",
    "c = a.copy()  # c and a are different arrays with the same contents\n",
    "c[2] = 19\n",
    "print(a,b,c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Operations on Lists\n",
    "Frequently we want to perform operations on lists\n",
    "\n",
    "Method | Description | Example\n",
    ":--- | :--- | :---\n",
    "`len` | Returns the number of entries in the list | `len(my_list)`\n",
    "`append(entry)` | Append an entry to the list | `my_list.append( 'another entry' )`\n",
    "`insert(index,entry)` | Insert an entry in the list | `my_list.insert(2,10)`\n",
    "`extend(list2)` | append a list | `my_list.append( ('a','b',x) )`\n",
    "`+` | append a list (same as `extend`) | `my_list + my_list`\n",
    "`remove(entry)` | Append an entry to the list | `my_list.remove( x )`\n",
    "`sort()` | sort the list - only works when the list is homogeneous | `x.sort()`\n",
    "`reverse()` | reverses the entries in the list | `my_list.reverse()`\n",
    "`pop(index)` | return and remove the element in the list at `index` | `my_list.pop(3)`\n",
    "`count(entry)` | return the number of occurences of `entry` in the list | `my_list.count(2)`\n",
    "`index(entry)` | return the index at which the first occurence of `entry` occurs | `my_list.index(x)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can also use the keyword `in` with a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Yes!\n"
     ]
    }
   ],
   "source": [
    "fruits = ['apple','pear','banana','payapa']\n",
    "if 'banana' in fruits:\n",
    "    print('Yes!')\n",
    "else:\n",
    "    print('No!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Indexing and Slicing Lists\n",
    "\n",
    "Indexing a list is done with the `[]` operator, and is 0-based (0 indicates the first entry in the list).  For example, given:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "fruits = ['apple','banana','grapefruit','orange']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "the following table shows various indexing operations:\n",
    "\n",
    "Operation | Result | Description\n",
    ":--- | :--- | :---\n",
    "`fruits[1]` | `banana` | Accesses the second element in the list\n",
    "`fruits[-1]` | `orange` | Accesses the last element in the list\n",
    "`fruits[-2]` | `grapefruit` | Accesses the second to last element in the list\n",
    "`fruits[:2]` | `[apple,banana]` | The first two elements in the list\n",
    "`fruits[1:3]`| `[banana,grapefruit]` | The second and third elements in the list\n",
    "\n",
    "The `:` operator allows us to perform _slicing_, accessing a subset of the entries in a list."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can also use slicing to replace elements of a list.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "y:  [3, 2]\n",
      "x:  [3, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "x = [1,2,3,4]\n",
    "y = x[1:3]  # note that this doesn't make a copy! y is refering to part of x\n",
    "y.reverse()\n",
    "print(\"y: \", y)\n",
    "x[0:2] = y\n",
    "print(\"x: \", x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Loops and Lists\n",
    "There are a few ways to loop over lists.  This is perhaps best illustrated by a few examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Iterating a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "apple\n",
      "banana\n",
      "orange\n"
     ]
    }
   ],
   "source": [
    "fruits = ['apple','banana','orange']\n",
    "for i in fruits:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Here, `i` is an _iterator_ that represents each entry in the list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Index loops\n",
    "Here we use the `range()` function, which builds a range space for the loop. This allows us to use `i` as an index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fruit 0 = apple\n",
      "Fruit 1 = banana\n",
      "Fruit 2 = orange\n"
     ]
    }
   ],
   "source": [
    "fruits = ['apple','banana','orange']\n",
    "for i in range(0,len(fruits)):\n",
    "    print('Fruit {} = {}'.format(i,fruits[i]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "`range(lo,hi)` creates a range of integers from __`lo`__ to __`hi-1`__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### List comprehensions\n",
    "List comprehensions can be used to quickly build lists conforming to specific patterns.  For example, if we wanted to build a list \n",
    "$$x_i=i^2, \\quad i=1\\ldots4$$\n",
    "we can do this by:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9]\n"
     ]
    }
   ],
   "source": [
    "x = [i**2 for i in range(1,4)]\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Similarly, to achieve $y_i=2^i, \\; i=1\\ldots 8$, we can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 4, 8, 16, 32, 64, 128, 256]\n"
     ]
    }
   ],
   "source": [
    "y = [2**i for i in range(1,9)]\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "List comprehensions provide a relatively simple syntax to build these types of lists."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Tuples\n",
    "In python, a _tuple_ is like a list, but has a few differences:\n",
    " * It is declared using `()` rather than `[]`\n",
    " * It is immutable - it cannot be changed once it is built\n",
    " \n",
    "Elements in a tuple are accessed in the same way as lists, using the `[]` operator.\n",
    "\n",
    "Generally, you will use lists rather than tuples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "# Numpy Arrays\n",
    "The list functionality in python is not as useful as it could be when it comes to numerical operations.  For example, you cannot perform mathematical operations on lists.\n",
    "This is where [numpy](https://docs.scipy.org/doc/numpy/reference/) comes in.\n",
    "\n",
    "Unlike a Python list, a Numpy arrays is _homogeneous_ - they contain entries of the same type (e.g., integer, real, complex).\n",
    "\n",
    "Here and below, we will assume that you have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "so that we can use __`np.`__ to shorten reference to numpy functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Constructing Numpy Arrays\n",
    "A numpy array is characterized by its _shape_ and the type of elements it contains."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(3,) [1 2 3]\n",
      "(2, 3) [[1 2 3]\n",
      " [4 5 6]]\n"
     ]
    }
   ],
   "source": [
    "x = np.array( [1,2,3] )             # a 1-dimensional row vector\n",
    "y = np.array( [ [1,2,3],[4,5,6] ] ) # a 2-dimensional matrix\n",
    "print(x.shape,x)\n",
    "print(y.shape,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Data Types\n",
    "You can explicitly specify the [type of the data](https://docs.scipy.org/doc/numpy-1.12.0/reference/arrays.dtypes.html#specifying-and-constructing-data-types) in the array.  Here are some of the common types you will use:\n",
    "\n",
    "Keyword | Description\n",
    ":---|:---\n",
    "[int](https://docs.python.org/dev/library/functions.html#int) | Integer\n",
    "[bool](https://docs.python.org/dev/library/functions.html#bool) | boolean (True/False)\n",
    "[float](https://docs.python.org/dev/library/functions.html#float) | Floating point (real)\n",
    "[complex](https://docs.python.org/dev/library/functions.html#complex) | Complex numbers\n",
    "[string](https://docs.python.org/dev/library/stdtypes.html#str) | string\n",
    "(other) | User-defined data types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1.+0.j 2.+0.j 3.+0.j]\n"
     ]
    }
   ],
   "source": [
    "x = np.array( [1,2,3], dtype=complex )\n",
    "print(x)   ## [ 1.+0.j  2.+0.j  3.+0.j ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Arrays over specified ranges\n",
    "Function | Description\n",
    ":--- | :---\n",
    "`linspace(lo,hi,npts)` | Builds a 1D array with a `npts` entries between `lo` and `hi`\n",
    "`arange(lo,hi,spacing)` | Builds a 1D array spaced with `spacing` starting at `lo` and ending near `hi`\n",
    "`logspace(lo,hi,npts)` | Builds a 1D array with `npts` points between $10^\\mathrm{lo}$ and $10^\\mathrm{hi}$\n",
    "[`meshgrid(x,y,...)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.meshgrid.html#numpy.meshgrid) | Given vectors specifying the range of each axis, builds a grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Other Constructors\n",
    "\n",
    "Command | Description\n",
    ":--- | :---\n",
    "`empty(shape)` | Build an empty array.\n",
    "`empty_like(a)` | Build an empty array shaped like `a`\n",
    "`eye(N)` | Build a 2D identity matrix with `N` rows and columns\n",
    "`ones(shape)` | build an array of ones with the specified `shape`\n",
    "`ones_like(a)` | Build an array of ones shaped like `a`\n",
    "`zeros(shape)` | build an array of zeros with the specified `shape`\n",
    "`zeros_like(a)` | Build an array of zeros shaped like `a`\n",
    "`full(shape,val)` | Build an array of the specified shape filled with `val`\n",
    "`full_like(a,val)` | Build an array shaped like `a` filled with `val`\n",
    "`random.random(shape)` | Build an array of random numbers with the specified shape\n",
    "\n",
    "Examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "x = np.ones( [1,3] )      # 3-element row vector\n",
    "y = np.empty_like( x )    # empty 3-element row vector\n",
    "z = np.zeros( [3,3] )     # 3x3 matrix\n",
    "p = np.full_like(z,np.pi) # 3x3 matrix full of 𝜋\n",
    "r = np.random.random([2,3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "All of these can have an additional `dypte` argument to specify the type of array to build (see [above](#Data-Types))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "For more information on many other ways of building arrays, see the [numpy docs](https://docs.scipy.org/doc/numpy/reference/routines.array-creation.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Numpy Matrices\n",
    "\n",
    "Most of the functions mentioned [above](#Other-Constructors) such as `zeros`, `ones`, `full`, `eye`, etc. will create matrices as well - just give the appropriate shape.\n",
    "\n",
    "Additionally, the [diag](https://docs.scipy.org/doc/numpy/reference/generated/numpy.diag.html#numpy.diag) function is very useful: \n",
    "  * `diag(v,k)` builds a matrix with `v` on its `k`<sup>th</sup> diagonal.  For example, the following builds a tridiagonal matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "n   = 5\n",
    "d   = np.full(n,-3)\n",
    "ud  = np.full(n-1,1)\n",
    "mat = np.diag(d,0) + np.diag(ud,1) + np.diag(ud,-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* `diag(m,k)` extracts the `k`<sup>th</sup> diagonal of the matrix `m`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "m  = np.random.random([5,5])\n",
    "# extract the main diagonal of m\n",
    "md = np.diag(m,0) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Manipulating Numpy Arrays\n",
    "There are many [array manipulation tools](https://docs.scipy.org/doc/numpy/reference/routines.array-manipulation.html#array-manipulation-routines).  Some of the more frequently used ones include:\n",
    "\n",
    "Function | Description\n",
    ":--- | :---\n",
    "[`reshape(a,shape)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html#numpy.reshape) | Reshape the data in `a` to a `shape`\n",
    "[`ndarray.flat`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.flat.html#numpy.ndarray.flat) | Obtain an iterator over the array\n",
    "[`transpose(a)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.transpose.html#numpy.transpose) | Transposes the array\n",
    "[`tile(a,nrep)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.tile.html#numpy.tile) | Tile `a` `nrep` times.  `nrep` can be an array.\n",
    "[`flip(a,axis)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.flip.html#numpy.flip) | Reverse the elements along the `axis` dimension of `a`.\n",
    "[`fliplr(a)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.fliplr.html#numpy.fliplr) | Flip the array in the left/right direction\n",
    "[`flipud(a)`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.flipud.html#numpy.flipud) | Flip the array in the up/down direction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Indexing and Slicing\n",
    "Numpy arrays are [indexed](https://docs.scipy.org/doc/numpy/reference/arrays.indexing.html) just like python lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0\n",
      "0.4444444444444444\n",
      "0.8888888888888888\n",
      "-2.0\n",
      "1.7777777777777777\n",
      "2.2222222222222223\n",
      "2.6666666666666665\n",
      "3.1111111111111107\n",
      "3.5555555555555554\n",
      "4.0\n"
     ]
    }
   ],
   "source": [
    "a = np.linspace(0,4,10)\n",
    "a[3] = -2\n",
    "for i in a:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can also slice arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.18403801 0.52644116 0.60619046 0.67778473]\n",
      " [0.16377359 0.53305578 0.83653102 0.45975799]\n",
      " [0.26448895 0.33457493 0.81659841 0.81666402]\n",
      " [0.14163167 0.46372596 0.6559922  0.39512843]]\n",
      "[1 3 5]\n"
     ]
    }
   ],
   "source": [
    "a = np.random.random([4,4])\n",
    "print(a)\n",
    "a[1:2,1:3]\n",
    "\n",
    "x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])\n",
    "print(x[1:7:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And slice from the \"back\" of arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[7 6 5]\n"
     ]
    }
   ],
   "source": [
    "a = np.arange(0,10,1)\n",
    "print(a[-3:4:-1])   # start at third-to last, end at fifth"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And slice all after:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5 6 7 8 9]\n"
     ]
    }
   ],
   "source": [
    "a = np.arange(0,10,1)\n",
    "print(a[5:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Mathematical Operations on Numpy Arrays\n",
    "\n",
    "Numpy arrays support typical mathematical operations like `+`, `-`, `*` (element-wise multipy), `/` (element-wise divide) and `**` (element-wise exponentiation) provided the arrays are the same shape.\n",
    "\n",
    "Numpy provides numerous [mathematical functions](https://docs.scipy.org/doc/numpy/reference/routines.math.html) that operate on arrays.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "x = np.linspace(-np.pi,np.pi)\n",
    "y = np.sin(x)\n",
    "z = x**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3628800"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n = 10\n",
    "np.prod( np.arange(1,n+1,1) )  # the factorial of n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Array Multiplication\n",
    "\n",
    "Given the arrays \n",
    "$A = \\left[\\begin{array}{cc}\n",
    "1 & 2\\\\\n",
    "3 & 4\n",
    "\\end{array}\\right]$,\n",
    "$b=\\left(\\begin{array}{c} 5\\\\6\\end{array}\\right)$,\n",
    "and\n",
    "$c=\\left(\\begin{array}{c} 7\\\\8\\end{array}\\right)$,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "A = np.array([[1,2],[3,4]])\n",
    "b = np.array([[5],[6]])\n",
    "c = np.array([[7],[8]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Elemental Multiplication\n",
    "For those coming from Matlab, the most familiar application of elemental multiplication is on arrays of the same shape.\n",
    "However, elemental multiplication does not require arrays to have the same shape.  For example:\n",
    "\n",
    "Operation | Description | Result \n",
    ":-------- | :---------- | :----- \n",
    "`A * A`   | Square elements in `A` | $\\begin{array}{cc} 1&4\\\\9&16\\end{array}$ \n",
    "`b * c`   | Element-wise multiplication of `b` and `c` | $\\begin{array}{c} 35\\\\48\\end{array}$\n",
    "`A * b`   | Multiply a 2x2 by a 2x1.  Results in a 2x2 | $\\begin{array}{cc} 5&10\\\\18&24\\end{array}$\n",
    "`b.transpose() * c` | Element-Multiply a row vector to a column vector | $\\begin{array}{cc} 35&42\\\\40&48\\end{array}$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Matrix Multiplication\n",
    "While `*` implies _elemental_ multiplication, `@` implies _matrix_ multiplication.\n",
    "\n",
    "Here are some example operations on the above arrays:\n",
    "\n",
    "Operation | Result\n",
    ":--- | :---\n",
    "`A @ b` | $\\begin{array}{c} 17 \\\\ 39 \\end{array}$\n",
    "`b.transpose() @ c` | 39\n",
    "`b@c` | error\n",
    "`b.transpose() @ A @ c` | 433"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## More useful stuff\n",
    "Among the other things that you should be aware of:\n",
    "  * Fourier Transform through the [`numpy.fft`](https://docs.scipy.org/doc/numpy/reference/routines.fft.html) module.\n",
    "  * Linear algebra through the [`numpy.linalg`](https://docs.scipy.org/doc/numpy/reference/routines.linalg.html) module.  This includes things like dot products, matrix products, norms, matrix decomositions, etc.\n",
    "  * [Statistics](https://docs.scipy.org/doc/numpy/reference/routines.statistics.html).\n",
    "  * [I/O](https://docs.scipy.org/doc/numpy/reference/routines.io.html) to help with reading/writing arrays from/to disk.\n",
    "  * Advanced [indexing tools](https://docs.scipy.org/doc/numpy/reference/routines.indexing.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "\n",
    "# Masking Numpy Arrays\n",
    "To do...  See docs [here](https://docs.scipy.org/doc/numpy/reference/maskedarray.html)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {
    "height": "372px",
    "width": "252px"
   },
   "number_sections": true,
   "sideBar": false,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "47px",
    "left": "71px",
    "right": "20px",
    "top": "1003px",
    "width": "159.359px"
   },
   "toc_section_display": false,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 1,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
