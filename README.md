# Overview
This repository holds resources related to [Jupyter notebooks](http://jupyter.org/).

# Viewing notebooks with nbviewer

To create a link to view a notebook:
  1. Open the notebook from this repository.  For example, the [Jupyter intro](JupyterIntro.ipynb).
  1. Click on the icon that looks like a page with a `</>` on it to generate the "raw" view of the file.  This can also be done by replacing `blob` with `raw` in the URL. For our [Jupyter intro notebook](JupyterIntro.ipynb), this would be:
```
  https://gitlab.multiscale.utah.edu/Teaching/public-tools/jupyter-tips/raw/master/JupyterIntro.ipynb
```
  1. Go to [nbviewer.jupyter.org](nbviewer.jupyter.org) and paste the URL from the previous step into the dialog box.
  1. Copy the resulting URL and link to that.  This should give something like:
  ```
  https://nbviewer.jupyter.org/urls/gitlab.multiscale.utah.edu/Teaching/public-tools/jupyter-tips/raw/master/JupyterIntro.ipynb
  ```
  to view the notebook or alternatively, to view it in slideshow mode:
  ```
  https://nbviewer.jupyter.org/format/slides/urls/gitlab.multiscale.utah.edu/Teaching/public-tools/jupyter-tips/raw/master/JupyterIntro.ipynb#/
  ```
__Notes__:
    * append `?flush_cache=true` to the URL to force a refresh of the notebook.
    * To obtain the URLs for slideshow vs. notebook mode, select the "notebook" or "present" icon in the top-right.

## Tips on creating slideshow-ready notebooks:
  1. In your notebook, go to View → Cell Toolbar → Slideshow
  1. In each cell, choose what kind of cell it should be: slide, sub-slide, fragment, etc.
By doing this, your notebook will render well in slideshow format.